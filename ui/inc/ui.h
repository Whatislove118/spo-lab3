

#ifndef LAB3_UI_H
#define LAB3_UI_H

void initialize_ui();
void string_up(char * message);
void string_down(char * message);
void print_message(char * message);

#endif
